
#include <stdio.h>

#include <pico.h>
#include <pico/scanvideo.h>
#include <pico/scanvideo/composable_scanline.h>
#include <pico/multicore.h>
#include <pico/sync.h>
#include <pico/stdlib.h>


#include <gd_db.h>

const uint64_t man1[] = {
   0x0000011111100000,
   0x0000100000010000,
   0x0000101010010000,
   0x0011100000011100,
   0x0100011111100010,
   0x1000000000000001,
   0x1001000000010001,
   0x1011000000010011,
   0x1001000000010001,
   0x1111111111111111,
   0x0100100000001001,
   0x0011100010000111,
   0x0000100010001100,
   0x0001110010000100,
   0x0010000100001100,
   0x0011111111111100
};

#define VGA_MODE vga_mode_320x240_60
extern const struct scanvideo_pio_program video_24mhz_composable;

static void frame_update_logic();
static void render_scanline( struct scanvideo_scanline_buffer *dest );

static gdImagePtr vga = 0;
static uint16_t vga_colors[gdMaxColors] = {0};
static uint16_t vga_pixels[320] = { 0 };

static uint frame = 0;

void gd2vga_colors( gdImagePtr im )
{
   for( uint i = 0; i < gdMaxColors; ++i )
   {
      vga_colors[i] = PICO_SCANVIDEO_PIXEL_FROM_RGB5(
                        gdImageRed( im, i )   >> 3,
                        gdImageGreen( im, i ) >> 3,
                        gdImageBlue( im, i )  >> 3
                        );
   }
}

gdImagePtr create_image( uint16_t width, uint16_t height )
{
   gdImagePtr im = gdImageCreate( width, height );

   int color0 = gdImageColorAllocate( im,   0,   0,   0 );
   int color1 = gdImageColorAllocate( im,   0,   0, 255 );
   int color2 = gdImageColorAllocate( im,   0, 255,   0 );

   gdImageFilledRectangle( im, 0,  0, (width - 1), height - 1, color1 );
   gdImageFilledRectangle( im, 0, (height / 2), (width - 1), height - 1, color2 );

   for( int y = 0 ; y < 16; ++y )
   {
      uint64_t b = 0x1000000000000000;
      for( int x = 0; x < 16; ++x, b >>= 4 )
      {
         if( b & man1[y] )
         {
            gdImageSetPixel( im, (im->sx - 16)/2 + x, (height / 2) - 16 + y, color0 );
         }
      }
   }

   return im;
}

// "Worker thread" for each core
void render_loop()
{
   static uint32_t prev_frame_num = 0;
   for(;;)
   {
      struct scanvideo_scanline_buffer *scanline_buffer = scanvideo_begin_scanline_generation( true );
      uint32_t frame_num = scanvideo_frame_number( scanline_buffer->scanline_id );
      if( frame_num != prev_frame_num )
      {
         prev_frame_num = frame_num;
         frame_update_logic();
      }
      render_scanline( scanline_buffer );
      scanvideo_end_scanline_generation( scanline_buffer );
   }
}

void frame_update_logic()
{
   frame++;
}

static inline uint16_t *raw_scanline_prepare(struct scanvideo_scanline_buffer *dest, uint width)
{
   assert(width >= 3);
   assert(width % 2 == 0);
   // +1 for the black pixel at the end, -3 because the program outputs n+3 pixels.
   dest->data[0] = COMPOSABLE_RAW_RUN | (width + 1 - 3 << 16);
   // After user pixels, 1 black pixel then discard remaining FIFO data
   dest->data[width / 2 + 2] = 0x0000u | (COMPOSABLE_EOL_ALIGN << 16);
   dest->data_used = width / 2 + 2;
   assert(dest->data_used <= dest->data_max);
   return (uint16_t *) &dest->data[1];
}

static inline void raw_scanline_finish(struct scanvideo_scanline_buffer *dest)
{
   // Need to pivot the first pixel with the count so that PIO can keep up
   // with its 1 pixel per 2 clocks
   uint32_t first = dest->data[0];
   uint32_t second = dest->data[1];
   dest->data[0] = (first & 0x0000ffffu) | ((second & 0x0000ffffu) << 16);
   dest->data[1] = (second & 0xffff0000u) | ((first & 0xffff0000u) >> 16);
   dest->status = SCANLINE_OK;
}


void __time_critical_func(render_scanline)( struct scanvideo_scanline_buffer *dest )
{
   int line = scanvideo_scanline_number( dest->scanline_id );
   uint8_t  *gdpixels   = vga->pixels[line];

   uint16_t *colour_buf = raw_scanline_prepare( dest, VGA_MODE.width );

   for( int x = 0; x < VGA_MODE.width; ++x )
   {
      *(colour_buf++) = vga_colors[*(gdpixels++)];
   }

   raw_scanline_finish( dest );
}


int main()
{
#if PICO_SCANVIDEO_48MHZ
   #warning 48MHz
   set_sys_clock_48mhz();
#endif
   // Re init uart now that clk_peri has changed
   //setup_default_uart();
   stdio_init_all();

   vga = create_image( VGA_MODE.width, VGA_MODE.height );
   gd2vga_colors( vga );

   for( int i = 3; i; --i )
   {
      printf( "start in %d...\n", i );
      sleep_ms( 1000 );
   }

   /* video setup and timimg must not run on core 1 */
   scanvideo_setup( &VGA_MODE );
   scanvideo_timing_enable( true );

   multicore_launch_core1( render_loop );

   uint64_t ts = time_us_64() + 1000000;
   for(;;)
   {
      if( time_us_64() > ts )
      {
         ts = time_us_64() + 1000000;
         printf( "frame: %d\n", frame );
      }
   }
   return 0;
}
