
$(info This Makefile is not required and for convenience only)
ifeq ($(PICO_SDK_PATH),)
PICO_SDK_PATH=$(shell readlink -f ../pico-sdk)
$(info Using local pico sdk at: $(PICO_SDK_PATH))
else
$(info Using global pico sdk at: $(PICO_SDK_PATH))
endif
PICO_SDK_PATH_CMAKE ?= -DPICO_SDK_PATH=$(PICO_SDK_PATH)

PICO_SDK_URL ?= https://github.com/raspberrypi/pico-sdk.git
PICO_BOARDS ?= pico pico_w waveshare_rp2040_lcd_0.96 vgaboard
BUILD_DIR ?= $(CURDIR)/build
JOBS ?= 4

all:

define build_board
   build_targets += build_$(1)
   clean_targets += clean_$(1)
   build_$(1): $$(PICO_SDK_PATH)/README.md
	cmake -S $$(CURDIR)/$(1) -B $$(BUILD_DIR)/$(1) $$(PICO_SDK_PATH_CMAKE) -DPICO_BOARD=$(1)
	make -C $$(BUILD_DIR)/$(1) -j $$(JOBS)
   clean_$(1):
	make -C $$(BUILD_DIR)/$(1) clean
endef

$(foreach board,$(PICO_BOARDS),$(eval $(call build_board,$(board))))

.PHONY: all clean distclean release setup-apt

all: $(build_targets)

clean: $(clean_targets)

distclean:
	rm -rf $(BUILD_DIR)

$(PICO_SDK_PATH)/README.md:
	mkdir -p $(PICO_SDK_PATH)
	git clone --recurse-submodules $(PICO_SDK_URL) $(PICO_SDK_PATH)

setup-apt:
	sudo apt install gdb-multiarch cmake gcc-arm-none-eabi libnewlib-arm-none-eabi libstdc++-arm-none-eabi-newlib cc65 microcom p7zip-full

