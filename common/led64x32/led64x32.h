
#ifndef LED64X32_H_
#define LED64X32_H_ LED64X32_H_

#include <stdint.h>

#include <gd.h>

typedef struct DS3231_tm {
   uint8_t tm_sec;
   uint8_t tm_min;
   uint8_t tm_hour;
   uint8_t tm_wday;
   uint8_t tm_mday;
   uint8_t tm_mon;
   uint16_t tm_year;
} DS3231_tm;

void ds3231_init();
void ds3231_get_time( DS3231_tm *tm );
void ds3231_set_time( DS3231_tm *tm );

void buzzer_init();
void buzzer_set( int val );

void keys_init();

void lightsensor_init();
uint16_t lightsensor_get();

void settings_write( void* memory, uint32_t size );
void settings_read( void* memory, uint32_t size );

void led_init();
gdImagePtr led_gdImageCreate();
gdImagePtr led_gdImageCreateTrueColor();
int led_set_gdImage( gdImagePtr im );
void led_set_brightness( uint bright );
typedef enum {
   BLACK  = 0,
   BLUE   = 1,
   GREEN  = 2,
   CYAN   = 3,
   RED    = 4,
   PURPLE = 5,
   YELLOW = 6,
   WHITE  = 7
} LED_COLORS;

#define CP printf( "%s:%d\n", __FILE__, __LINE__ );

#endif
