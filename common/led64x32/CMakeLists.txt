cmake_minimum_required(VERSION 3.13)

project(led64x32)
pico_sdk_init()

set(BUILD_SHARED_LIBS 0)
set(BUILD_STATIC_LIBS 1)
set(LED64X32_STATIC "led64x32")
set(LED64X32_INCLUDE_DIR ${CMAKE_CURRENT_SOURCE_DIR} CACHE PATH "led64x32")
set(LED64X32_SRC_FILES
   ds3231.c
   gdLED.c
   misc.c
   )

add_library(${LED64X32_STATIC} STATIC
   ${LED64X32_SRC_FILES}
   )

target_link_libraries(${LED64X32_STATIC} PRIVATE
   pico_stdlib
   hardware_adc
   hardware_i2c
   hardware_flash
   gd
   )

target_include_directories(${LED64X32_STATIC} PRIVATE
   ${CMAKE_CURRENT_SOURCE_DIR}
   ${GD_INCLUDE_DIRS}
   )

