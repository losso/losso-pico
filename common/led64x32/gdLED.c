
#include "pico/stdlib.h"
#include <hardware/timer.h>

#include <gd.h>
#include <assert.h>

#include "gpios.h"

#include "led64x32.h"

static struct repeating_timer led_draw_callback_handle;
bool led_draw_callback(struct repeating_timer *t);

static struct repeating_timer led_light_callback_handle;
bool led_light_callback(struct repeating_timer *t);

static gdImagePtr gdCurrentImage = 0;
static gdImagePtr gdCurrentImageTrueColor = 0;

static uint brightness = 1;

void put_image_row( uint8_t y );
void put_image_row_truecolor( uint8_t y, uint8_t frame );

#define PWM_FRAMES 16

gdImagePtr led_gdImageCreate()
{
   int color;
   gdImagePtr im = gdImageCreate( 64, 32 );

   color = gdImageColorAllocate( im,   0,   0,   0 );
   assert( color == 0 );
   color = gdImageColorAllocate( im,   0,   0, 255 );
   assert( color == 1 );
   color = gdImageColorAllocate( im,   0, 255,   0 );
   assert( color == 2 );
   color = gdImageColorAllocate( im,   0, 255, 255 );
   assert( color == 3 );
   color = gdImageColorAllocate( im, 255,   0,   0 );
   assert( color == 4 );
   color = gdImageColorAllocate( im, 255,   0, 255 );
   assert( color == 5 );
   color = gdImageColorAllocate( im, 255, 255,   0 );
   assert( color == 6 );
   color = gdImageColorAllocate( im, 255, 255, 255 );
   assert( color == 7 );

#if DARK_COLORS
   color = gdImageColorAllocate( im,   0,   0,   0 );
   assert( color == 8 );
   /* use second black as transparency color */
   gdImageColorTransparent( im, color );
   color = gdImageColorAllocate( im,   0,   0, 127 );
   assert( color == 9 );
   color = gdImageColorAllocate( im,   0, 127,   0 );
   assert( color == 10 );
   color = gdImageColorAllocate( im,   0, 127, 127 );
   assert( color == 11 );
   color = gdImageColorAllocate( im, 127,   0,   0 );
   assert( color == 12 );
   color = gdImageColorAllocate( im, 127,   0, 127 );
   assert( color == 13 );
   color = gdImageColorAllocate( im, 127, 127,   0 );
   assert( color == 14 );
   color = gdImageColorAllocate( im, 127, 127, 127 );
   assert( color == 15 );
#endif

   return im;
}

gdImagePtr led_gdImageCreateTrueColor()
{
   gdImagePtr im = gdImageCreateTrueColor ( 64, 32 );
   return im;
}

static void gpio_init_out( uint gpio )
{
   gpio_init( gpio );
   gpio_set_dir( gpio, GPIO_OUT );
}


static void gpio_put_rgb( bool value )
{
   gpio_put( GPIO_R1, value );
   gpio_put( GPIO_G1, value );
   gpio_put( GPIO_B1, value );
   gpio_put( GPIO_R2, value );
   gpio_put( GPIO_G2, value );
   gpio_put( GPIO_B2, value );
}


void led_init()
{
   const int MaxLed = 64;
   uint8_t l, y;

   lightsensor_init();

   gpio_init_out( GPIO_R1 );
   gpio_init_out( GPIO_G1 );
   gpio_init_out( GPIO_B1 );
   gpio_init_out( GPIO_R2 );
   gpio_init_out( GPIO_G2 );
   gpio_init_out( GPIO_B2 );

   gpio_init_out( GPIO_A0 );
   gpio_init_out( GPIO_A1 );
   gpio_init_out( GPIO_A2 );
   gpio_init_out( GPIO_A3 );
   gpio_init_out( GPIO_A4 );

   gpio_init_out( GPIO_CLK );
   gpio_init_out( GPIO_STB );
   gpio_init_out( GPIO_OE );

   gpio_put( GPIO_OE, 1 );
   gpio_put( GPIO_STB, 0 );
   gpio_put( GPIO_CLK, 0 );

   int C12_C13[16] = {0, 1, 1, 1, 1, 1, 1, 1, 1, 3, 1, 1, 1, 1, 1, 1};

   for( l = 0; l < MaxLed; ++l )
   {
      y = l % 16;

      gpio_put_rgb( 0 );
      if( C12_C13[y] & 1 )
      {
         gpio_put_rgb( 1 );
      }

      if( l > (MaxLed - 12) )
      {
         gpio_put( GPIO_STB, 1 );
      }
      else
      {
         gpio_put( GPIO_STB, 0 );
      }

      gpio_put( GPIO_CLK, 1 );
      sleep_us( 2 );
      gpio_put( GPIO_CLK, 0 );
   }
   gpio_put( GPIO_STB, 0 );
   gpio_put( GPIO_CLK, 0 );

   /* send data to control register 12 */
   for( l = 0; l < MaxLed; ++l )
   {
      y = l % 16;

      gpio_put_rgb( 0 );
      if( C12_C13[y] & 2 )
      {
         gpio_put_rgb( 1 );
      }

      if( l > (MaxLed - 13) )
      {
         gpio_put( GPIO_STB, 1 );
      }
      else
      {
         gpio_put( GPIO_STB, 0 );
      }

      gpio_put( GPIO_CLK, 1 );
      sleep_us( 2 );
      gpio_put( GPIO_CLK, 0 );
   }
   gpio_put( GPIO_STB, 0 );
   gpio_put( GPIO_CLK, 0 );

   gdCurrentImage = 0;

   // Note: smaller delays than 5 don't work -- related to this bug/fix?
   // https://github.com/raspberrypi/pico-sdk/pull/1488/commits/f593e463866dcaf950e25641c1961005420b2145
   add_repeating_timer_us(5, led_draw_callback, NULL, &led_draw_callback_handle);
}


int led_set_gdImage( gdImagePtr im )
{
   if( (gdImageSX(im) != 64) || (gdImageSY(im) != 32) )
   {
      return 2;
   }
   if( gdImageTrueColor(im) )
   {
      gdCurrentImage = 0;
      gdCurrentImageTrueColor = im;
   }
   else
   {
      gdCurrentImage = im;
      gdCurrentImageTrueColor = 0;
   }
   return 0;
}


void led_set_brightness( uint bright )
{
    if( (bright >= 1) && (bright <= 10) )
    {
       brightness = bright;
    }
}


bool led_draw_callback( struct repeating_timer *t )
{
   static uint8_t y = 0;
   static uint8_t frame = 0;

   if( !gdCurrentImage && !gdCurrentImageTrueColor )
   {
      return true;
   }

   if( ++y >= 16 )
   {
      y = 0;
      if( ++frame >= PWM_FRAMES ) frame = 0;
   }

   gpio_put( GPIO_OE, 1 );

   if ( gdCurrentImage )
   {
       put_image_row( y );
   }
   else
   {
       put_image_row_truecolor( y, frame );
   }

   gpio_put( GPIO_A0, y & 0x01 );
   gpio_put( GPIO_A1, y & 0x02 );
   gpio_put( GPIO_A2, y & 0x04 );
   gpio_put( GPIO_A3, y & 0x08 );
   gpio_put( GPIO_A4, y & 0x10 );

   gpio_put( GPIO_STB, 1 );
   gpio_put( GPIO_STB, 0 );

#if 1
   /* disable on auto light */
   gpio_put( GPIO_OE, 0 );
#else
   /* misusing repeating timer for a single shot */
   add_repeating_timer_us(101-brightness*10, led_light_callback, NULL, &led_light_callback_handle);
#endif

   return true;
}

void put_image_row( uint8_t y )
{
   for( uint8_t x = 0; x < 64; ++x )
   {
      uint8_t rgb;
      gpio_put( GPIO_CLK, 0 );

      rgb = gdImageGetPixel( gdCurrentImage, x, y );
      gpio_put( GPIO_R1, rgb & 0x04 );
      gpio_put( GPIO_G1, rgb & 0x02 );
      gpio_put( GPIO_B1, rgb & 0x01 );

      rgb = gdImageGetPixel( gdCurrentImage, x, y+16 );
      gpio_put( GPIO_R2, rgb & 0x04 );
      gpio_put( GPIO_G2, rgb & 0x02 );
      gpio_put( GPIO_B2, rgb & 0x01 );

      gpio_put( GPIO_CLK, 1 );
   }
}

const int widths[] = {
    0, 0, // alternating pulse widths for r/g/b value 0/15 etc.
    0, 0,
    0, 1,
    0, 1,
    1, 1,
    1, 1,
    1, 2,
    2, 2,
    2, 2,
    2, 3,
    3, 3,
    3, 3,
    3, 4,
    4, 4,
    6, 6,
    16,16
};

void put_image_row_truecolor( uint8_t y, uint8_t total_frame )
{
   int r, g, b;
   uint8_t frame = total_frame & ((PWM_FRAMES/2)-1);
   int odd = (frame < total_frame);
   for( uint8_t x = 0; x < 64; ++x )
   {
      int rgb;
      gpio_put( GPIO_CLK, 0 );
      
      rgb = gdImageGetTrueColorPixel( gdCurrentImageTrueColor, x, y );
      r = (rgb & 0xf00000) >> 20;
      g = (rgb & 0x00f000) >> 12;
      b = (rgb & 0x0000f0) >>  4;
      gpio_put( GPIO_R1, frame < widths[ r*2 + odd ] );
      gpio_put( GPIO_G1, frame < widths[ g*2 + odd ] );
      gpio_put( GPIO_B1, frame < widths[ b*2 + odd ] );

      rgb = gdImageGetTrueColorPixel( gdCurrentImageTrueColor, x, y+16 );
      r = (rgb & 0xf00000) >> 20;
      g = (rgb & 0x00f000) >> 12;
      b = (rgb & 0x0000f0) >>  4;
      gpio_put( GPIO_R2, frame < widths[ r*2 + odd ] );
      gpio_put( GPIO_G2, frame < widths[ g*2 + odd ] );
      gpio_put( GPIO_B2, frame < widths[ b*2 + odd ] );

      gpio_put( GPIO_CLK, 1 );
   }
}

bool led_light_callback( struct repeating_timer *t )
{
   gpio_put( GPIO_OE, 0 );
   /* make this timer single shot */
   return false;
}
