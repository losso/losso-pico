cmake_minimum_required(VERSION 3.13)

project(tremor)
pico_sdk_init()

set(BUILD_SHARED_LIBS 0)
set(BUILD_STATIC_LIBS 1)
set(TREMOR_STATIC "tremor")
set(TREMOR_INCLUDE_DIR ${CMAKE_CURRENT_SOURCE_DIR} CACHE PATH "tremor")
set(TREMOR_SRC_FILES
   tremor/bitwise.c
   tremor/codebook.c
   tremor/dsp.c
   tremor/floor0.c
   tremor/floor1.c
   tremor/floor_lookup.c
   tremor/framing.c
   tremor/info.c
   tremor/mapping0.c
   tremor/mdct.c
   tremor/misc.c
   tremor/res012.c
   tremor/vorbisfile.c
   tremor_in_rom.c
   )
add_library(${TREMOR_STATIC} STATIC
   ${TREMOR_SRC_FILES}
   )
# will not work on Pico, as code is ARM and not Thumb-2
#target_compile_definitions(${TREMOR_STATIC} PRIVATE
#   _ARM_ASSEM_
#   )

target_link_libraries(${TREMOR_STATIC} PRIVATE
   )

target_include_directories(${TREMOR_STATIC} PRIVATE
   ${CMAKE_CURRENT_SOURCE_DIR}/tremor
   ${CMAKE_CURRENT_SOURCE_DIR}
   )
