
#include "gd_db.h"

/* this is taken from gdImageFilledEllipse and slightly edited */
void gdImageFilledEllipseFast( gdImagePtr im, int mx, int my, int w, int h, int c )
{
   int x=0,mx1=0,mx2=0,my1=0,my2=0;
   long aq,bq,dx,dy,r,rx,ry,a,b;
   int i,x1,x2;
   int old_y2;

   a=w>>1;
   b=h>>1;

   x1 = mx - a;
   if( x1 < 0 )
   {
      x1 = 0;
   }
   x2 = mx + a;
   if( x2 > im->sx )
   {
      x2 = im->sx - 1;
   }
   if( im->trueColor )
   {
      for( x = x1; x <= x2; ++x )
      {
         im->tpixels[my][x] = c;
      }
   }
   else
   {
      for( x = x1; x <= x2; ++x )
      {
         im->pixels[my][x] = c;
      }
   }

   mx1 = mx-a;
   my1 = my;
   mx2 = mx+a;
   my2 = my;

   aq = a * a;
   bq = b * b;
   dx = aq << 1;
   dy = bq << 1;
   r  = a * bq;
   rx = r << 1;
   ry = 0;
   x = a;
   old_y2=-2;
   while (x > 0)
   {
      if (r > 0)
      {
         my1++;
         my2--;
         ry +=dx;
         r  -=ry;
      }
      else /* (r <= 0) */
      {
         x--;
         mx1++;
         mx2--;
         rx -=dy;
         r  +=rx;
      }
      if(old_y2!=my2)
      {
         int x1 = (mx1 < 0) ? 0 : mx1;
         int x2 = (mx2 >= im->sx) ? im->sx - 1 : mx2;
#if 0
printf( "mx1=%d,mx2=%d,my1=%d,my2=%d,xsize=%d,ysize=%d\n", mx1, mx2, my1, my2, im->sx, im->sy );
#endif

         if( (my1 >= 0) && (my1 < im->sy) )
         {
            if( im->trueColor )
            {
               for( i = x1; i <= x2; ++i )
               {
                  im->tpixels[my1][i] = c;
               }
            }
            else
            {
               for( i = x1; i <= x2; ++i )
               {
                  im->pixels[my1][i] = c;
               }
            }
         }

         if( (my2 >= 0) && (my2 < im->sy) )
         {
            if( im->trueColor )
            {
               for( i = x1; i <= x2; ++i )
               {
                  im->tpixels[my2][i] = c;
               }
            }
            else
            {
               for( i = x1; i <= x2; ++i )
               {
                  im->pixels[my2][i] = c;
               }
            }
         }
      }
      old_y2 = my2;
   }
}
