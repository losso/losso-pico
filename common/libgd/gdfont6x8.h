
#ifndef LIBOPC_GDFONT6X8APPLE1_H
#define LIBOPC_GDFONT6X8APPLE1_H LIBOPC_GDFONT6X8APPLE1_H

#ifdef __cplusplus
extern "C"
{
#endif

#include "gd.h"

extern gdFontPtr gdFont6x8;
BGD_DECLARE(gdFontPtr) gdFontGet6x8(void);

#ifdef __cplusplus
}
#endif

#endif
