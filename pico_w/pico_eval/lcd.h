
#ifndef LCD_H
#define LCD_H LCD_H

#include <gd.h>

#define LCD_WIDTH  480
#define LCD_HEIGHT 320

#define LCD_RST_PIN        15	
#define LCD_DC_PIN         8
#define LCD_CS_PIN         9
#define LCD_CLK_PIN        10
#define LCD_BKL_PIN        13
#define LCD_MOSI_PIN       11
#define LCD_MISO_PIN       12
#define TP_CS_PIN          16
#define TP_IRQ_PIN         17
#define SD_CS_PIN          22

#define SPI_PORT           spi1

void lcd_init();
void lcd_set_gdColor( gdImagePtr im );
void lcd_set_gdImage( gdImagePtr im );

#endif
