cmake_minimum_required(VERSION 3.13)

if(${CMAKE_SOURCE_DIR} STREQUAL ${CMAKE_BINARY_DIR})
   message(FATAL_ERROR "In-source build detected!")
endif()

include(../pico_sdk_import.cmake)
include(../pico_extras_import.cmake)
include(../common/common.cmake)

# initialize the SDK based on PICO_SDK_PATH
# note: this must happen before project()
project(pico_projects)
pico_sdk_init()

set(CMAKE_C_FLAGS_RELEASE "${CMAKE_C_FLAGS_RELEASE} -Wunused-variable")

add_common_directory(misc)
add_common_directory(libgd)

# projects
add_subdirectory(dev_hid_composite)
add_subdirectory(flashcount)

